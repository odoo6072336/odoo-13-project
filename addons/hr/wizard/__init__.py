# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import hr_plan_wizard
from . import hr_departure_wizard
from . import hr_image_check
from . import get_data_cam
from . import hr_add_newv