import cv2
import numpy as np
import sqlite3
import os
from odoo import api, fields, models
import time
from PIL import Image

import numpy as np
import tensorflow as tf

from tensorflow.keras.preprocessing.image import load_img,img_to_array

import os
import faiss


import logging
_logger = logging.getLogger(__name__)

class add_newv(models.TransientModel):
    _name = "hr.add.newv"


    def get_embedding(self, model, face_pixels):
        # to float 32
        face_pixels = face_pixels.astype('float32')
        # standardize pixel values tren cac kenh
        mean, std = face_pixels.mean(), face_pixels.std()
        face_pixels = (face_pixels - mean) / std
        # chuyen doi shape
        samples = np.expand_dims(face_pixels, axis=0)
        # make prediction to get embedding
        yhat = model.predict(samples)
        return yhat[0]

    model = tf.keras.models.load_model('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/weights/facenet_keras.h5')


    def add_newv(self):
        record = self.env.context['active_ids'][0]
        print(record)

        #Prepare Training Data
        x_train_new=[]
        y_train_new=[]

        record = str(record)

        path_check = '/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/'+record+'/'
        if os.path.exists(path_check) == True:
            person_folders=os.listdir('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/'+record+'/')
            #print(person_folders)
            for image_name in person_folders:
                if image_name!=".DS_Store":
                    print(image_name)
                    img=load_img('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/'+record+'/'+image_name,target_size=(160,160))
                    img=img_to_array(img)
                    img_encode= self.get_embedding(self.model, img)
                    x_train_new.append(img_encode.tolist()) # => (128, )
                    # print(np.squeeze(img_encode).shape) # => (128, )
                    # print(type(np.squeeze(img_encode).tolist())) # => [0, 1, 2, 3, ...., 128]
                    y_train_new.append(record)
            print("Add new employee: Done")

            #chuyển đổi thành mảng numpy
            x_train_new=np.array(x_train_new)
            y_train_new=np.array(y_train_new)

            x_train_new = x_train_new.astype(np.float32)
            y_train_new = y_train_new.astype(np.int64)

            y_train = np.load('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/embeddings-folder/a_temp_test_add/train_labels.npy')
            if record not in y_train:
                # thêm vào index faiss đã có
                ind = faiss.read_index("/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_v2.index")
                ind.add(x_train_new)
                # có thể overwrite old file
                faiss.write_index(ind, "/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_v2.index")

                # thêm vào y_train
                y_train = np.append(y_train, y_train_new)
                # overwrite old file
                np.save('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/embeddings-folder/a_temp_test_add/train_labels',y_train)
            else:
                print("da ton tai")
        else:
            print("Thư mục chưa tồn tại.")









