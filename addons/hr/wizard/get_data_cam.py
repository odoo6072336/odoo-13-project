# import cv2
# import numpy as np
# import sqlite3
# import os
# from facenet_pytorch import MTCNN

# class Getdata_fromCam(object):
#     _name = "hr.source.get"

#     def __init__(self, mtcnn):
#         self.mtcnn = mtcnn


#     def draw(self, frame, boxes, probs, landmarks):
#         "draw boundings box, probs and landmarks"

#         for box, prob, landmark in zip(boxes, probs, landmarks):

#             try:
#                 box = box.astype('int')
#                 landmark = landmark.astype('int')

#                 # 1. draw HCN
#                 ##truyền vào hình ảnh, hình ảnh ở dầy chỉ là khung
#                 cv2.rectangle(frame,
#                             (box[0], box[1]), #điểm thứ nhất (góc trái trên)
#                             (box[2], box[3]), #điểm thứ 2 (rộng và cao của bounding box)
#                             (0,0,255), #color của HCN
#                             thickness=2) #độ dày

#                 # 2. hiển thị xác suất
#                 ## (frame, str, vị trí, font chữ, fontScale, màu, độ dày, lineType)
#                 cv2.putText(frame, str(prob), (box[2], box[1]),
#                             cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),
#                             2, cv2.LINE_AA)

#                 # 3. Draw landmark
#                 ## (frame, điểm, bán kính, màu, độ dày)
#                 cv2.circle(frame, tuple(landmark[0]), 3, (0,0,255), -1)
#                 cv2.circle(frame, tuple(landmark[1]), 3, (0,0,255), -1)
#                 cv2.circle(frame, tuple(landmark[2]), 3, (0,0,255), -1)
#                 cv2.circle(frame, tuple(landmark[3]), 3, (0,0,255), -1)
#                 cv2.circle(frame, tuple(landmark[4]), 3, (0,0,255), -1)
#             except Exception as e:
#                 print(e)
#                 pass

#         return frame

#     def run(self):
#         cap = cv2.VideoCapture(0)

#         sampleNum = 0

#         while True:
#             ret, frame = cap.read()

#             gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

#             try:
#                 # detect box, probs, landmarks
#                 boxes, probs, landmarks = self.mtcnn.detect(frame, landmarks=True)

#                 self.draw(frame, boxes, probs, landmarks)

#                 if not os.path.exists('dataSet'):
#                     os.makedirs('dataSet')

#                 if not os.path.exists('dataSet/User'+str(_employee_get(self))):
#                     os.makedirs('dataSet/User'+str(_employee_get(self)))


#                 sampleNum += 1

#                 for box in boxes:
#                     box = box.astype('int')
#                     cv2.imwrite('/Users/haonguyen/odoo-project/odoo/addons/hr_attendence_project/static/face_recognition/dataset/User ' + str(_employee_get(self)) + '/User' + str(_employee_get(self)) + '_' + str(sampleNum) + '.jpg',
#                                 gray[box[1]:box[3], box[0]:box[2]])

#             except Exception as e:
#                 print(e)
#                 pass

#             cv2.imshow('Drop-detector', frame)
#             cv2.waitKey(1)

#             # Hơn 20 image thì dừng
#             if sampleNum > 1 or (cv2.waitKey(1) & 0xFF == ord('q')):
#                 break

#         cap.release()
#         cv2.destroyAllWindows()