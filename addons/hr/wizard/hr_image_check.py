import cv2
import os
from odoo import api, fields, models
import time
from PIL import Image
import numpy as np
import sys
sys.path.append( '/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/api_model' )
from face_detection import return_face

import logging
_logger = logging.getLogger(__name__)

import requests
import base64
from io import BytesIO


class get_data(models.TransientModel):
    _name = "hr.image.check"


    # path0 = fields.Text("Path image 0")
    # path1 = fields.Text("Path image 1")
    # path2 = fields.Text("Path image 2")
    # path3 = fields.Text("Path image 3")
    image_url = fields.Char(string="Image URL")
    image = fields.Binary(
        string="Image",
        compute="_compute_image",
        store=True,
        attachment=False
    )
    id_em = fields.Integer("ID_em", store=True)

    @api.depends("image_url")
    def _compute_image(self):

        for record in self:
            image = None
            if record.image_url:
                image = self.fetch_image_from_url(record.image_url)
            record.update({"image": image, })

    def fetch_image_from_url(self, url):
        # Lấy url trỏ đến hình ảnh, rồi trả về chuỗi base64 có thể store được.
        data = ""

        try:
            # Python 3
            with open(url, "rb") as image_file:
                data = base64.b64encode(image_file.read())
        except Exception as e:
            _logger.warn("There was a problem requesting the image from URL %s" % url)
            logging.exception(e)

        return data

    def run_file(self):

        record = self.env.context['active_ids'][0]
        cap = cv2.VideoCapture(0)
        time.sleep(3.0)
        sampleNum = 0
        while True:
            ret, frame = cap.read() # returns ret (0/1 if the camera is working) and frames
            # print(cap.isOpened(),ret,frame)
            # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # cv2.imshow('Lấy ảnh khuôn mặt', frame)

            pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
            pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image
            faces, h, w = return_face(frame)

            if not os.path.exists('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/' + str(record)):
                os.makedirs('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/' + str(record))
            else:
                print("exist")
                break

            try:


                # draw(frame, boxes, probs, landmarks)

                # # data-set
                # if not os.path.exists('/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set/'+str(record)):
                #     os.makedirs('/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set/'+str(record))

                # # data-set-demo
                if not os.path.exists('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/'+str(record)):
                    os.makedirs('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/'+str(record))

                # cv2.imshow('Drop-detector', frame)


                for face in range(faces.shape[2]):
                    #to draw faces on image
                    confidence = faces[0, 0, face, 2]
                    x, y, x1, y1 = 0, 0, 0, 0
                    if confidence > 0.9:
                        box = faces[0, 0, face, 3:7] * np.array([w, h, w, h])
                        (x, y, x1, y1) = box.astype("int")
                        cv2.rectangle(frame, (x, y), (x1, y1), (0, 0, 255), 1)

                        # drop_face
                        crop_img = pil_img.crop((x, y,
                                            x1, y1))
                        # if key == ord('s'):
                        # cv2.imwrite('/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set/' + str(record) + '/' + str(record) + ' ' + '(' + str(sampleNum) + ')' + '.jpg',
                        #             crop_img)
                        # key = cv2.waitKey(1)
                        # if  key == ord('s'):
                        crop_img.save('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/' + str(record) + '/' + str(record) + ' ' + '(' + str(sampleNum) + ').jpg')


                        #     continue
                        # else:
                        #     continue
                        # if sampleNum == 0:
                        self.image_url = '/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set-demo/' + str(record) + '/' + str(record) + ' ' + '(' + str(sampleNum) + ').jpg'
                        # with open(t, "rb") as image_file:
                        #     data = base64.b64encode(image_file.read())
                        # self.image = data
                        self.id_em = record
                        # im = Image.open(BytesIO(base64.b64decode(data)))
                        # im.save('/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set-demo/image1.png', 'PNG')
                        # elif sampleNum == 1:
                        #     self.path1 = '/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set/' + str(record) + '/' + str(record) + ' ' + '(' + str(sampleNum) + ')' + '.jpg'
                        # elif sampleNum == 2:
                        #     self.path2 = '/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set/' + str(record) + '/' + str(record) + ' ' + '(' + str(sampleNum) + ')' + '.jpg'
                        # elif sampleNum == 3:
                        #     self.path3 = '/Users/haonguyen/odoo-project/odoo/addons/hr_attendance_facenet_faiss/static/src/data-set/' + str(record) + '/' + str(record) + ' ' + '(' + str(sampleNum) + ')' + '.jpg'
                        sampleNum += 1

            except Exception as e:
                print(e)
                pass

            # cv2.imshow('Drop-detector', frame)
            # cv2.waitKey(1)
            if sampleNum > 0:
                break
            # if sampleNum > 4 or (cv2.waitKey(1) & 0xFF == ord('q')):
            #     break

        cap.release()
        cv2.destroyAllWindows()

        print("Done")











