

# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError

class Benhnhan(models.Model):
    _name = "sinh.vien"
    _description = "sinh.vien"

    name = fields.Char('Tên Sinh viên', required=True)
    ma_sinhvien = fields.Char('Mã Số Sinh viên')
    gioi_tinh = fields.Selection([
        ('Nam', 'Nam'),
        ('Nu', 'Nữ'),
        ('khac', 'Khác'),
    ], string='Giới tính', default='nam')

    dien_thoai = fields.Char('Điện thoại')

    ngay_sinh = fields.Date('Ngày sinh') 
    
    diem_10 = fields.Float("Điểm 10")

    vi_pham_quy_che = fields.Text('Vi phạm Quy chế')


    # Ma MH
    company_id = fields.Many2one('res.company', 'Company')

    department_id = fields.Many2one('hr.department', 'Department', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    diem_khoa_nhap = fields.Float("Điểm khoa nhập")