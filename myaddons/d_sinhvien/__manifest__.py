# -*- coding: utf-8 -*-

{
    'name': "Sinh vien",
    'summary': """Quan ly sinh vien""",
    'description': """Quan Ly sinh vien""",
    'author': "haokah",
    
    'category': 'Uncategorized',
    'version': '0.1',
    'depends':[
        # warning
    ],
    'data': [
        # Đăng ký access right.
        'security/ir.model.access.csv',
        # Đăng ký view.
        'views/sinhvien_view.xml',
    ],
    'installable': True,
    'application': True,
}