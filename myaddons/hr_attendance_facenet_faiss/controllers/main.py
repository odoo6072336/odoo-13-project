from urllib import response
import odoo
import logging
import json
__logger = logging.getLogger(__name__)

import requests
import json
from odoo import http, _, exceptions
from odoo.tests import Form
import werkzeug.wrappers
import xlsxwriter
from odoo.http import content_disposition, request


class API_MyAttendance(http.Controller):    
    # API đọc dữ liệu từ model
    @http.route(['/hr_attendance/<dbname>/<id>'], type='http', auth="none", sitemap=False, cors='*', csrf=False)
    def pet_handler(self, dbname, id, **kw):
        model_name = "hr.attendance"
        try:
            registry = odoo.modules.registry.Registry(dbname)
            with odoo.api.Environment.manage(), registry.cursor() as cr:
                env = odoo.api.Environment(cr, odoo.SUPERUSER_ID, {})
                rec = env[model_name].search([('id', '=', int(id))], limit=1)
                response = {
                    "status": "ok",
                    "content": {
                        "check_in": rec.check_in.strftime('%Y-%m-%dT%H:%M:%S'),
                        "check_out": rec.check_out.strftime('%Y-%m-%dT%H:%M:%S'),
                        "employee_id": rec.employee_id.id,
                        
                    }
                }
        except Exception:
            response = {
                "status": "error",
                "content": "not found"
            }
        return json.dumps(response)

# get 
# class testingRestApiAttendance_get(http.Controller):
    # @http.route(['/api/attendance_get/'], type = "http", auth="public", methods=['GET'], csrf=False)
    # def testing_RestApi_attendance_get(self, **params):
    #     model_name = "don.thuoc"
    #     get_id = params.get("id")
    #     attendance = request.env[model_name].sudo().search([('id', '=', get_id)])
    #     dict_attendance = {}
    #     data_attendance = []
    #     for h in attendance: 
    #         dict_attendance = {"employee_id": h.employee_id.id, 
    #                         "employee_name": h.employee_id.display_name,
    #                         "check_in": h.check_in.strftime('%Y-%m-%dT%H:%M:%S'),
    #                         "check_out": h.check_out.strftime('%Y-%m-%dT%H:%M:%S'),
    #                         "worked_hours": h.worked_hours
    #                         }
    #         data_attendance.append(dict_attendance)
            
    #     data ={
    #         'status': 200,
    #         'message': "success",
    #         'response': data_attendance
    #     }

    #     try:
    #         return werkzeug.wrappers.Response(
    #             status=200,
    #             content_type='application/json; charset=utf-8',
    #             response=json.dumps(data)
    #         )
    #     except Exception:
    #         return werkzeug.wrappers.Response(
    #             status=400,
    #             content_type='application/json; charset=utf-8',
    #             headers=[('Access-Control-Allow-Origin', '*')],
    #             response=json.dumps({
    #                 'error': 'Error',
    #                 'error_descrip': 'Error Description',
    #             })
    #         )

    # @http.route(['/api/attendance_post/'], type = "json", auth="public", methods=['POST'], csrf = False)
    # def testing_RestApi_attendance_post(self, **params):
    #     attendance = params.get("")

