# from ast import literal_eval

# import logging
# import json
# __logger = logging.getLogger(__name__)

# import json
# from odoo import http, _, exceptions
# from odoo.tests import Form
# import werkzeug.wrappers
# import xlsxwriter
# from facenet_pytorch import MTCNN


# import faiss

# import sys

# sys.path.append( 'addons/hr_attendance_facenet_faiss/static/src/api_model' )

# from webcam import Webcam
# from model_faiss_mtcnn import faceDetect

# import tensorflow as tf
# mtcnn = MTCNN()


# sys.path.append('addons/hr_attendance_facenet_faiss')
# from run_api import checkinout





# class API_Model(http.Controller):

#     # @http.route(['/web_stream'], type = "http", auth="public", website=True, methods=['GET'])
#     # def web_stream(self):
        
#     #     # return "hello world"
#     #     return request.render("hr_attendance_facenet_faiss.index")
    

#     def read_from_webcam(self, model_emb, ind):
#         webcam = Webcam()
#         model = faceDetect(mtcnn)
#         object_checkinout = checkinout()
#         list_id_e = []
#         dem = 0
#         while True:
#             # đọc ảnh từ class Webcam
#             # khi chúng ta gọi một generator chúng ta phải thêm next
#             image = next(webcam.get_frame())

#             # trước khi return frame cho trang web ta nhận diện qua model faiss.
#             id_e , image = model.run(image, model_emb, ind)
#             if id_e:
#                 dem +=1
#                 list_id_e.append(id_e)
            
#             if dem >5:
#                 # lấy employee được recog nhiều nhất.
#                 many_e = max(set(list_id_e),key = list_id_e.count)

#                 if many_e != "Stranger":
#                     #check_in
#                     if  object_checkinout.start_checkin < object_checkinout.time_now < object_checkinout.end_checkin:
#                         object_checkinout.check_in(many_e)
#                         # print("Check in done")
#                         dem = 0
#                         list_id_e = []

#                     #check_out
#                     elif object_checkinout.start_checkout < object_checkinout.time_now < object_checkinout.end_checkout:
#                         object_checkinout.check_out(many_e)
#                         # print("Check out done")
#                         dem = 0
#                         list_id_e = []
#                     else:
#                         dem = 0
#                         list_id_e = []
            
#             # return cho web bằng lệnh yield (yield statement)
#             # yield + pathdata
#             # trả về cho đúng dạng multipath ở hàm return_frame_for_web bên dưới.
#             yield b'Content-Type: image/jpg\r\n\r\n' + image + b'\r\n--frame\r\n'

    

#     model_emb = tf.keras.models.load_model('addons/hr_attendance_facenet_faiss/static/src/weights/facenet_keras.h5')
#     # IP
#     # ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/knn.index") 
#     # L2
#     # ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/knn_l2.index") 

#     # L2 test add_newv
#     # ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_v2.index") 

#     # L2 test add_newv with embedding_image_no_22.py
#     # ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_v2.index") 


#     @http.route(['/image_feed'], type = "http", auth="public", website=True, methods=['GET'])
#     def image_feed(self):
#         ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_v2.index") 

#         # return ở dạng mimitype: multipart
#         return werkzeug.wrappers.Response(self.read_from_webcam(self.model_emb, ind), mimetype="multipart/x-mixed-replace; boundary=frame")
