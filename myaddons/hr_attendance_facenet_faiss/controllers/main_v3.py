# from ast import literal_eval

# import odoo
# import logging
# import json
# __logger = logging.getLogger(__name__)

# import requests
# import json
# from odoo import http, _, exceptions
# from odoo.tests import Form
# import werkzeug.wrappers
# import xlsxwriter
# from odoo.http import content_disposition, request, route


# import sys

# sys.path.append( 'addons/hr_attendance_facenet_faiss/static/src/api_model' )

# from face_search import search_face
# from model_faiss import faceDetect


# import base64

# from datetime import datetime
# URL = "http://localhost:8989"

# import cv2
# # sys.path.append( 'addons/hr_attendance_facenet_faiss/static/src/api_model' )

# # from webcam import Webcam
# # from model_faiss import faceDetect
# _logger = logging.getLogger(__name__)

# import faiss
# import tensorflow as tf
# from PIL import Image



# class API_model_v2(http.Controller):
    
#     # @http.route(['/hinh_anh'], type = "http", auth="public", website=True, methods=['GET'])
#     # def image_feed_test(self):
#     #     # return ở dạng mimitype: multipart
#     #     # return werkzeug.wrappers.Response(self.read_from_webcam(self.model_emb, self.ind), mimetype="multipart/x-mixed-replace; boundary=frame")
#     #     return request.render("hr_attendance_facenet_faiss.index_test")

#     model_emb = tf.keras.models.load_model('addons/hr_attendance_facenet_faiss/static/src/weights/facenet_keras.h5')
#     ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/knn_l2.index") 
    

#     @http.route(['/hinh_anh/get_id'], type = "http", auth="public", website=True, methods=['GET'])
#     def image_feed_test_get_id(self, **params):
#         get_url = params.get("path_image")
#         model = faceDetect()
#         data = ""
#         frame = cv2.imread(get_url) #sử dụng oepn để convert to PIL image => để crop image
#         pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
#         pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image

#         faces = model.detector.detect_faces(frame)
#         print("Results:")
#         for face in faces:
#             bounding_box = face['box']
        
#             # draw face frame
            
#             # drop_face
#             crop_img = pil_img.crop((bounding_box[0], bounding_box[1],
#                                 bounding_box[0]+bounding_box[2], bounding_box[1]+bounding_box[3]))

#             # đưa crop_img vào search_face
#             id_e = search_face(crop_img, self.model_emb, self.ind)


            
#             # display similar
#             # cv2.putText(image,str(sim), (bounding_box[2], bounding_box[1]),
#             #         cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)


#             data ={
#                 'status': 200,
#                 'message': "success",
#                 'response': id_e
#             }

#         try:
#             return werkzeug.wrappers.Response(
#                 status=200,
#                 content_type='application/json; charset=utf-8',
#                 response=json.dumps(data)
#             )
#         except Exception:
#             return werkzeug.wrappers.Response(
#                 status=400,
#                 content_type='application/json; charset=utf-8',
#                 headers=[('Access-Control-Allow-Origin', '*')],
#                 response=json.dumps({
#                     'error': 'Error',
#                     'error_descrip': 'Error Description',
#                 })
#             )