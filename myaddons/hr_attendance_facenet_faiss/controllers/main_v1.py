from ast import literal_eval
from email import message
from urllib import response
import odoo
import logging
import json
__logger = logging.getLogger(__name__)

import requests
import json
from odoo import http, _, exceptions
from odoo.tests import Form
import werkzeug.wrappers
import xlsxwriter
from odoo.http import content_disposition, request, route


import datetime

import sys

sys.path.append('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss')


from run_api import checkinout

object_checkinout = checkinout()

start_checkin = object_checkinout.start_checkin
end_checkin = object_checkinout.end_checkin
start_checkout = object_checkinout.start_checkout




class API_Attendance(http.Controller):
    @http.route(['/api/attendance_get/'], type = "http", auth="public", methods=['GET'], csrf=False)
    def RestApi_attendance_get(self, **params):
        model_name = "hr.attendance"
        get_id = params.get("employee_id")
        # attendance = request.env[model_name].sudo().search([])
        attendance = request.env[model_name].sudo().search([("employee_id.id", "=", get_id)])

        dict_attendance = {}
        data_attendance = []

        for h in attendance:
            dict_attendance = {"employee_id": h.employee_id.id,
                                "employee_name": h.employee_id.display_name,
                                "check_in": h.check_in.strftime('%Y-%m-%dT%H:%M:%S'),
                                "check_out": h.check_out.strftime('%Y-%m-%dT%H:%M:%S'),
                                "worked_hours": h.worked_hours}
            data_attendance.append(dict_attendance)
        
        data ={
            'status': 200,
            'message': "success",
            'response': data_attendance
        }

        try:
            return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
                response=json.dumps(data)
            )
        except Exception:
            return werkzeug.wrappers.Response(
                status=400,
                content_type='application/json; charset=utf-8',
                headers=[('Access-Control-Allow-Origin', '*')],
                response=json.dumps({
                    'error': 'Error',
                    'error_descrip': 'Error Description',
                })
            )

    # get display name
    @http.route(['/api/get_name'], type = "http", auth="public", methods=['GET'], csrf=False)
    def RestApi_get_name(self, **params):
        model_name = "hr.employee"
        get_id = params.get("employee_id")
        # attendance = request.env[model_name].sudo().search([])
        employee = request.env[model_name].sudo().search([("id", "=", get_id)])

        dict_employee = {}
        data_employee = []

        for h in employee:
            dict_employee = {"id": h.id,
                            "employee_name": h.name
                                }
            data_employee.append(dict_employee)

        data ={
            'status': 200,
            'message': "success",
            'response': data_employee
        }

        # try:
        #     return werkzeug.wrappers.Response(
        #         status=200,
        #         content_type='application/json; charset=utf-8',
        #         response=json.dumps(data)
        #     )
        # except Exception:
        #     return werkzeug.wrappers.Response(
        #         status=400,
        #         content_type='application/json; charset=utf-8',
        #         headers=[('Access-Control-Allow-Origin', '*')],
        #         response=json.dumps({
        #             'error': 'Error',
        #             'error_descrip': 'Error Description',
        #         })
        #     )

        tmp = "Stranger"

        if data['response']:
            return data['response'][0]['employee_name']
        else:
            return tmp


    @http.route(['/api/attendance_post_check'], type = "http", auth="public", methods=['POST'], csrf=False)
    def RestApi_attendance_post(self, **params):
        time_now = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
        get_employee_id = params.get("epl")
        check_type = literal_eval(params.get('in'))
        # truy xuất employee
        if get_employee_id != "Stranger":
            employee_obj = request.env['hr.employee'].sudo().search([('id', '=', get_employee_id)])

            if int(check_type) == 1:
                # KIỂM TRA employee_id đã tồn tại chưa ?. (employee đã được tạo)
                if employee_obj.id:
                    # kiểm tra trong buổi sáng đó employee đã check in chưa ?
                    # search trong bảng hr_attendance
                    attendance_id = request.env['hr.attendance'].sudo().search([
                        ('employee_id.id', '=', get_employee_id),
                        ('check_in', '>=', start_checkin),
                        ('check_in', '<=', end_checkin)])

                    # nếu chưa thấy xuất hiện id này (id của attendance) thì cho check_in
                    # nếu có id nghĩa là người người đã check_in
                    if attendance_id.id == False:
                        vals_header={
                            "employee_id": employee_obj.id, "check_in": time_now,
                        }
                        
                        # create new check_in
                        new_data = request.env['hr.attendance'].sudo().create(vals_header)
                        print("\n")
                        print("***"+str(employee_obj.id)+": Check_in Done")
                        print("\n")
                        return json.dumps({'message': '***'+str(employee_obj.id)+": Check_in Done"})
                    else:
                        print("\n")
                        print("***"+str(employee_obj.id)+": Checked in")
                        print("\n")
                        return json.dumps({'message': "Fail: ***"+str(employee_obj.id)+": checked in"})

                # không tồn tại employee này.
                else:
                    print("\n")
                    print("***Employee no exist")
                    print("\n")
                    return json.dumps({'message': "Fail: Employee no exist"})


            #check_out
            else:
                # start_checkout = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d 15:00:00")
                # end_checkout = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d 20:00:00")

                # KIỂM TRA employee_id đã tồn tại chưa ?. (employee đã được tạo)
                if employee_obj.id:
                    # kiểm tra trong buổi sáng đó employee đã check in chưa ?
                    # search trong bảng hr_attendance

                    # tìm kiểm employee đã check in mà chưa check out
                    # ràng buộc thời gian check_in sẽ ở bên run_api.py
                    attendance_id = request.env['hr.attendance'].sudo().search([
                            ('employee_id.id', '=', get_employee_id),
                            # nếu đã check in trong ngày hôm nay
                            # nghĩa là lớn hơn thời gian start_checkin
                            ('check_in', '>=', start_checkin),
                            ('check_in', '<=', end_checkin),
                            ('check_out', '=', False)
                            # chưa check_out
                            ])
                    # Nếu tồn tại người đã check in mà chưa check out => cho check_out
                    if attendance_id.id:
                        attendance_id.check_out = time_now
                        print("\n")
                        print("***"+str(employee_obj.id)+": Check_out Done")
                        print("\n")
                        return json.dumps({'message':"***"+str(employee_obj.id)+": Check_out Done"})
                    else:
                        attendance_id = request.env['hr.attendance'].sudo().search([
                            ('employee_id.id', '=', get_employee_id),
                            # nếu đã check in trong ngày hôm nay
                            # nghĩa là lớn hơn thời gian start_checkin và bé hơn thời gian check_out
                            ('check_in', '>=', start_checkin),
                            ('check_in', '<=', end_checkin),
                            # chưa check_out
                            ('check_out', '>=', start_checkout)
                            ])
                        if attendance_id.id:
                            print("\n")
                            print("***"+str(employee_obj.id)+": Checked out")
                            print("\n")
                            return json.dumps({'message': "Fail: ***"+str(employee_obj.id)+": Checked out"})
                        else:
                            print("\n")
                            print("***"+str(employee_obj.id)+": Chua check in")
                            print("\n")
                            return json.dumps({'message': "Fail: ***"+str(employee_obj.id)+": Chua check in"})
                else:
                    print("\n")
                    print("Employee no exist")
                    print("\n")
                    return json.dumps({'message': "Fail: Employee no exist"})
        else:
            print("\n")
            print("Employee no exist")
            print("\n")
            return json.dumps({'message': "Fail: Employee no exist"})


    @http.route(['/api/check_checked_inout'], type = "http", auth="public", methods=['GET'], csrf=False)
    def RestApi_attendance_check_checked_inout(self, **params):
        get_employee_id = params.get("epl")
        check_type = literal_eval(params.get('in'))
        # truy xuất employee
        status_check = ""
        if get_employee_id != "Stranger":
            employee_obj = request.env['hr.employee'].sudo().search([('id', '=', get_employee_id)])

            if int(check_type) == 1:
                # KIỂM TRA employee_id đã tồn tại chưa ?. (employee đã được tạo)
                if employee_obj.id:
                    # kiểm tra trong buổi sáng đó employee đã check in chưa ?
                    # search trong bảng hr_attendance
                    attendance_id = request.env['hr.attendance'].sudo().search([
                        ('employee_id.id', '=', get_employee_id),
                        ('check_in', '>=', start_checkin),
                        ('check_in', '<=', end_checkin)])

                    # nếu chưa thấy xuất hiện id này (id của attendance) thì cho check_in
                    # nếu có id nghĩa là người người đã check_in
                    if attendance_id.id == False:
                        # create new check_in
                        print("\n")
                        print("***"+str(employee_obj.id)+": Check_in Done")
                        print("\n")

                        status_check = "0"
                        
                        
                        # return json.dumps({'message': '***'+str(employee_obj.id)+": Chua_check_in"})
                        return status_check
                    else:
                        print("\n")
                        print("***"+str(employee_obj.id)+": Checked in")
                        print("\n")
                        status_check = "1"
                        # return json.dumps({'message': "Fail: ***"+str(employee_obj.id)+": checked in"})
                        return status_check

                # không tồn tại employee này.
                else:
                    print("\n")
                    print("***Employee no exist")
                    print("\n")
                    # return json.dumps({'message': "Fail: Employee no exist"})
                    # return
            else:
                if employee_obj.id:
                    # kiểm tra trong buổi sáng đó employee đã check in chưa ?
                    # search trong bảng hr_attendance

                    # tìm kiểm employee đã check in mà chưa check out
                    # ràng buộc thời gian check_in sẽ ở bên run_api.py


                    attendance_id = request.env['hr.attendance'].sudo().search([
                            ('employee_id.id', '=', get_employee_id),
                            # nếu đã check in trong ngày hôm nay
                            # nghĩa là lớn hơn thời gian start_checkin và bé hơn thời gian check_out
                            ('check_in', '>=', start_checkin),
                            ('check_in', '<=', end_checkin),
                            # chưa check_out
                            ('check_out', '=', False)
                            ])
                    # Nếu tồn tại người đã check in mà chưa check out => cho check_out
                    if attendance_id.id:
                        print("\n")
                        print("***"+str(employee_obj.id)+": Chua check out")
                        print("\n")
                        status_check = "1"
                        
                        # return json.dumps({'message':"***"+str(employee_obj.id)+": Check_out Done"})
                        return status_check
                    else:
                        attendance_id2 = request.env['hr.attendance'].sudo().search([
                            ('employee_id.id', '=', get_employee_id),
                            # nếu đã check in trong ngày hôm nay
                            # nghĩa là lớn hơn thời gian start_checkin và bé hơn thời gian check_out
                            ('check_in', '>=', start_checkin),
                            ('check_in', '<=', end_checkin),
                            # chưa check_out
                            ('check_out', '>=', start_checkout)
                            ])
                        if attendance_id2.id:
                            print("\n")
                            print("***"+str(employee_obj.id)+": Checked out")
                            print("\n")
                            status_check = "0"
                        else:
                            print("\n")
                            print("***"+str(employee_obj.id)+": Chưa check_in buoi sang")
                            print("\n")
                            status_check = "2"
                             
                        # return json.dumps({'message': "Fail: ***"+str(employee_obj.id)+": Checked out hoac chua check in"})
                        return status_check
                else:
                    print("\n")
                    print("Employee no exist")
                    print("\n")
                    return json.dumps({'message': "Fail: Employee no exist"})