# -*- coding: utf-8 -*-

{
    'name': "Attendence by Facenet faiss",
    'summary': """Attendence by Facenet faiss""",
    'description': """Attendence by Facenet faiss""",
    'author': "haokah",
    
    'category': 'Uncategorized',
    'version': '0.1',
    'depends':[
        # warning
        'base',
        'hr',
        'hr_attendance',
    ],
    'data': [
        # Đăng ký access right.
        # 'views/attendance_view_new.xml',
        # Đăng ký view. 
        'views/index.xml',  
        'views/index_test.xml',
    ],
    'qweb': [
        'static/src/xml/attendance_face.xml',
        # 'static/src/web_html/index.html',
    ],
    'installable': True,
    'application': True,
}