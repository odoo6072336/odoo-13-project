from logging import exception
from urllib import request




import requests
from datetime import datetime
import datetime

import sys

sys.path.append( '/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/api_model' )


URL = "http://localhost:7979"



class checkinout():
    def __init__(self):
        # self.time_now = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S")

        self.start_checkin = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d 05:23:00")
        self.end_checkin = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d 05:25:00")
        
        self.start_checkout = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d 05:26:00")
        self.end_checkout = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d 05:28:00")


    def check_in(self, many_e):
        self.many_e = many_e
        request_url  = "%s/api/attendance_post_check?epl=%s&in='1'" % (URL, str(self.many_e))
        if request_url:
            requests.post(request_url)
            print(self.many_e+"-check_in Done")
    
    def check_out(self, many_e):
        self.many_e = many_e
        request_url  = "%s/api/attendance_post_check?epl=%s&in='0'" % (URL, str(self.many_e))
        if request_url:
            requests.post(request_url)
            print(self.many_e+"-check_out Done")

# class faceDetect(object):
#     def __init__(self, mtcnn):
#         self.detector = mtcnn

#     def run(self):

#         dem = 0

#         cap = cv2.VideoCapture(0) #webcam của laptop => 0
#         time.sleep(2.0)
#         # print(cap.isOpened())

#         list_id_e = []

#         #vòng lặp đọc các frame image từ camera
#         while True:
#             ret, frame = cap.read() # returns ret (0/1 if the camera is working) and frames
#             try:
#                 pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
#                 pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image

#                 faces = self.detector.detect_faces(frame)
#                 print("Results:")
#                 for face in faces:
#                     bounding_box = face['box']
                
#                     # draw face frame
#                     cv2.rectangle(frame,(bounding_box[0], bounding_box[1]),
#                                 (bounding_box[0]+bounding_box[2], bounding_box[1]+bounding_box[3]),
#                                 (0,204,0),2)
                    
#                     # drop_face
#                     crop_img = pil_img.crop((bounding_box[0], bounding_box[1],
#                                         bounding_box[0]+bounding_box[2], bounding_box[1]+bounding_box[3]))

#                     # đưa crop_img vào search_face
#                     id_e = search_face(crop_img)

#                     if id_e:
#                         dem +=1
#                         list_id_e.append(id_e)

#                     # puttext: id + name của employee
#                     name_display = "%s/api/get_name?employee_id=%s" % (URL, str(id_e))
                    
#                     name = requests.get(name_display)
#                     if name:
#                         t = name.text
#                         info = str(id_e) +"-"+ t


#                     cv2.putText(frame, str(info), (bounding_box[0], bounding_box[1]),
#                             cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv2.LINE_AA)
                    
#                     # display similar
#                     # cv2.putText(image,str(sim), (bounding_box[2], bounding_box[1]),
#                     #         cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

#             except Exception as e:
#                 print(e)
#                 pass

#             # tên window
#             # hiển thị khung kết quả.
#             cv2.imshow('Face-detector', frame)
#             cv2.waitKey(1)

#             # nhấn phím q to finish
#             if cv2.waitKey(1) & 0xFF == ord('q'):
#                 break

#             # 5 frames
#             if dem > 5:
#                 # lấy employee được recog nhiều nhất.
#                 many_e = max(set(list_id_e),key = list_id_e.count)

#                 object_checkinout = checkinout(many_e)
#                 #check_in
#                 if  object_checkinout.start_checkin < object_checkinout.time_now < object_checkinout.end_checkin:
#                     object_checkinout.check_in()
#                     dem = 0
#                     list_id_e = []

#                 #check_out
#                 elif object_checkinout.start_checkout < object_checkinout.time_now < object_checkinout.end_checkout:
#                     object_checkinout.check_out()
#                     dem = 0
#                     list_id_e = []
#                 else:
#                     dem = 0
#                     list_id_e = []




#         cap.release()
#         cv2.destroyAllWindows()


    

# def main():

#     mtcnn = MTCNN()
#     rf = faceDetect(mtcnn)
#     rf.run()




# if __name__ == '__main__':
#     main()