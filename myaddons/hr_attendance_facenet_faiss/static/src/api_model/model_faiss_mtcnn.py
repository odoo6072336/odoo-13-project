# import cv2
# import numpy as np
# from face_search import search_face
# from PIL import Image
# import requests # get display_name
# import sys

# sys.path.append('addons/hr_attendance_facenet_faiss')
# from run_api import checkinout

# URL = "http://localhost:8989"



# class faceDetect(object):
#     def __init__(self, mtcnn):
#         self.mtcnn = mtcnn

#     def run(self, frame, model_emb, ind):
#         object_checkinout = checkinout()
#         pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
#         pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image
#         # get the start time
#         img = frame.copy()
#         h, w = img.shape[:2]
#         img = cv2.resize(img, (640, 360),  interpolation = cv2.INTER_AREA)
#         h = h/360
#         w = w/640
#         img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
#         boxes, confs = self.mtcnn.detect(img)

#         id_e = ""
#         print("Results:")
#         try:
#             if boxes is not None:
#                 for box, conf in zip(boxes, confs):
#                     if conf > 0.9:
#                         box = box * np.array([w, h, w, h]) 
#                         box = box.astype('int')
#                         cv2.rectangle(frame,
#                                     (box[0], box[1]), #điểm thứ nhất (góc trái trên)
#                                     (box[2], box[3]), #điểm thứ 2 (rộng và cao của bounding box)
#                                     (0,0,255), #color của HCN
#                                     thickness=1) #độ dày

#                         # drop_face
#                         crop_img = pil_img.crop((box[0], box[1],
#                                             box[2], box[3]))

#                         # đưa crop_img vào search_face
#                         id_e = search_face(crop_img, model_emb, ind)
                            
                            
#                         if id_e != "Stranger":
#                             name_display = "%s/api/get_name/?employee_id=%s" % (URL, str(id_e))
#                             name = requests.get(name_display)
#                             # kiểm tra thời gian
#                             if  object_checkinout.start_checkin < object_checkinout.time_now < object_checkinout.end_checkin:
#                                 # kiểm tra status
#                                 status_api = "%s/api/check_checked_inout?epl=%s&in='1'" % (URL, str(id_e))
#                                 status = requests.get(status_api)
#                                 if name:
#                                     t = name.text
#                                     if status.text == "1":
#                                         info = t + "- Checked in"
#                                     else:
#                                         info = t + "- Chua check in"
#                                 else:
#                                     info = id_e
#                             elif object_checkinout.start_checkout < object_checkinout.time_now < object_checkinout.end_checkout:
#                                 status_api = "%s/api/check_checked_inout?epl=%s&in='0'" % (URL, str(id_e))
#                                 status = requests.get(status_api)
#                                 if name:
#                                     t = name.text
#                                     if status.text == "1":
#                                         info = t + "- Chua check out"
#                                     elif status.text == "0":
#                                         info = t + "- Checked out"
#                                     else:
#                                         info = t + "- Chua check in"
#                                 else:
#                                     info = id_e
#                             else:
#                                 if name:
#                                     t = name.text
#                                     info = t + "- Chua toi gio cham cong"
#                                 else:
#                                     info = id_e + "- Chua toi gio cham cong"
#                         else:
#                             info = id_e
#                         cv2.putText(frame, str(info), (box[0], box[1]),
#                         cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv2.LINE_AA)

#         except Exception as e:
#             print(e)
#             pass

#         return id_e, cv2.imencode('.jpg', frame)[1].tobytes()
