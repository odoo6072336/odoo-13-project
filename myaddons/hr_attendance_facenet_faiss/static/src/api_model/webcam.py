import cv2
# from datetime import datetime

class Webcam():
    def __init__(self):
        self.vid = cv2.VideoCapture(0)

    def get_frame(self):
        
        if not self.vid.isOpened():
            return
        
        while True:
            ret, frame = self.vid.read()

            # # Thêm datetime
            # font = cv2.FONT_HERSHEY_SIMPLEX
            # org = (50, 50)
            # fontScale = 1
            # color = (255, 0, 0)
            # thickness = 2

            # time = datetime.now().strftime("%H:%M:%S")

            # frame = cv2.putText(frame, time, org, font,
            #                   fontScale, color, thickness, cv2.LINE_AA)

            # yield frame sẽ bị lỗi vì chưa encode
            # yield frame
            yield frame