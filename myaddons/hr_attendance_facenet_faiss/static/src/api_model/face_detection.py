import cv2


modelFile = "/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/weights/DNN/res10_300x300_ssd_iter_140000.caffemodel"
configFile = "/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/weights/DNN/deploy.prototxt.txt"

def return_face(frame):
    net = cv2.dnn.readNetFromCaffe(configFile, modelFile)

    img = frame
    h, w = img.shape[:2]
    # normalization
    blob = cv2.dnn.blobFromImage(img, 1.0,
                (300, 300), (104.0, 117.0, 123.0), swapRB = False)
    # 1.0: không biến đổi gì (vì nhân với 1.0)
    net.setInput(blob)
    faces = net.forward()

    return faces, h, w
