import cv2
import numpy as np
from face_search import search_face
from PIL import Image
from face_detection import return_face

URL = "http://localhost:7979"

import requests # get display_name
import sys
sys.path.append('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss')
from run_api import checkinout
import datetime
class faceDetect():

    def run(self, frame, model_emb, ind):
        object_checkinout = checkinout()
        pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image
        # get the start time
        faces, h, w = return_face(frame)
        id_e = ""
        print("Results:")
        for face in range(faces.shape[2]):
            #to draw faces on image
            confidence = faces[0, 0, face, 2]
            x, y, x1, y1 = 0, 0, 0, 0
            if confidence > 0.9:
                box = faces[0, 0, face, 3:7] * np.array([w, h, w, h])
                (x, y, x1, y1) = box.astype("int")
                cv2.rectangle(frame, (x, y), (x1, y1), (0, 0, 255), 1)
            
                # drop_face
                crop_img = pil_img.crop((x, y,
                                    x1, y1))

                # đưa crop_img vào search_face
                id_e = search_face(crop_img, model_emb, ind)
                
                if id_e != "Stranger":
                    name_display = "%s/api/get_name/?employee_id=%s" % (URL, str(id_e))
                    name = requests.get(name_display)
                    if name:
                        t = name.text
                        # kiểm tra thời gian
                        time_now = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
                        if  object_checkinout.start_checkin <= time_now <= object_checkinout.end_checkin:
                            # kiểm tra status
                            status_api = "%s/api/check_checked_inout?epl=%s&in='1'" % (URL, str(id_e))
                            status = requests.get(status_api)
                            
                            if status.text == "1":
                                info = t + "- Checked in"
                            else:
                                info = t + "- Chua check in"
                            
                        elif object_checkinout.start_checkout <= time_now <= object_checkinout.end_checkout:
                            status_api = "%s/api/check_checked_inout?epl=%s&in='0'" % (URL, str(id_e))
                            status = requests.get(status_api)
                            
                            if status.text == "1":
                                info = t + "- Chua check out"
                            elif status.text == "0":
                                info = t + "- Checked out"
                            else:
                                info = t + "- Chua check in"
                        else:
                            t = name.text
                            info = t + "- Chua toi gio cham cong"
                    else:
                        info = id_e
                else:
                    info = id_e
                cv2.putText(frame, str(info), (x, y),
                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv2.LINE_AA)

                break



        return id_e, cv2.imencode('.jpg', frame)[1].tobytes()
