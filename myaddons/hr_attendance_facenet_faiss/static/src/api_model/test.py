import cv2
from flask import Flask, request
import sys

sys.path.append( '/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/api_model' )

from webcam import Webcam
from model_faiss import faceDetect
from face_search import search_face
import faiss
from PIL import Image
import tensorflow as tf


app = Flask(__name__)
@app.route("/", methods=['GET', 'POST'])
def home_page():
    if request.method =="POST":
        model = faceDetect()
        model_emb = tf.keras.models.load_model('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/weights/facenet_keras.h5')
        ind = faiss.read_index("/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/index-folder/knn.index")
  

        image = request.files['image']
        image.save("/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/img/baoanh_test.jpg")
        frame = cv2.imread("/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/img/baoanh_test.jpg")
        pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image

        faces = model.detector.detect_faces(frame)
        print("Results:")
        for face in faces:
            bounding_box = face['box']
        
            # draw face frame
            
            # drop_face
            crop_img = pil_img.crop((bounding_box[0], bounding_box[1],
                                bounding_box[0]+bounding_box[2], bounding_box[1]+bounding_box[3]))

            # đưa crop_img vào search_face
            id_e = search_face(crop_img, model_emb, ind)


        return id_e
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)