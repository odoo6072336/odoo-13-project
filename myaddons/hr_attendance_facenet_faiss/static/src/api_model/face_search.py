# pip install faiss

# pip install torchvision
import numpy as np
import tensorflow as tf
import cv2



from tensorflow.keras.preprocessing.image import load_img,img_to_array


# pip install tensorflow


import os

# pip install pandas
import pandas as pd

os.environ['KMP_DUPLICATE_LIB_OK']='True'

# Read file KNN index
# metadata_0.parquet :chứa đường dẫn hình ảnh, chú thích và siêu dữ liệu
# df = pd.read_parquet("addons/hr_attendance_facenet_faiss/static/src/embeddings-folder/metadata/metadata_0.parquet")

# image_list chứa đường dẫn hình ảnh.
# image_list = df["image_path"].tolist()

# ind: chứa chỉ mục KNN ảnh database được tạo bởi AutoFaiss
# ind = faiss.read_index("addons/hr_attendance_facenet_faiss/static/src/index-folder/knn.index")

# # 1000 person
# y_train=np.load('addons/hr_attendance_facenet_faiss/static/src/embeddings-folder/train_labels.npy')

# # 12+ person


def get_embedding(model_emb, face_pixels):
    # scale pixel values
    face_pixels = face_pixels.astype('float32')
    # standardize pixel values across channels (global)
    mean, std = face_pixels.mean(), face_pixels.std()
    face_pixels = (face_pixels - mean) / std
    # transform face into one sample
    samples = np.expand_dims(face_pixels, axis=0)
    # make prediction to get embedding
    yhat = model_emb.predict(samples)
    return yhat[0]

# model = tf.keras.models.load_model('addons/hr_attendance_facenet_faiss/static/src/weights/facenet_keras.h5')

# Search image
def search_face(image, model_emb, ind):
    y_train=np.load('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/embeddings-folder/a_temp_test_add/train_labels.npy')
    try:
        image.save('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/temp/crop_img.jpg')
    except Exception as e:
        print(e)
        pass
    # t2=load_img('addons/hr_attendance_facenet_faiss/static/src/temp/crop_img.jpg') # target Facenet expect #PIL (224, 224)
    t=load_img('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/temp/crop_img.jpg',target_size=(160,160)) # target Facenet expect #PIL (224, 224)

    t= img_to_array(t)
    t= get_embedding(model_emb, t)

    t = t.reshape(1, 128).astype('float32')
    os.remove('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/temp/crop_img.jpg')


    #Dùng phương thức search của KNN inđex để tìm ra ảnh tương đồng nhất với ảnh truy vấn
    # D: độ tương đồng của ảnh tìm được với ảnh truy vấn
    # I: đường dẫn của ảnh đó trong database

    D, I = ind.search(t, 1)
    
    if D[0][0] < 10:
        name = y_train[I[0][0]]
        print("Name: ",name)
        print("Sim:",D[0][0])
    else:
        print("Sim:",D[0][0])
        name = "Stranger"
        print("Name:",name)

    return  name
