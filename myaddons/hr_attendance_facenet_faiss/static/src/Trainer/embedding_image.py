import numpy as np
import tensorflow as tf

from tensorflow.keras.preprocessing.image import load_img,img_to_array

import os
import csv


def get_embedding(model, face_pixels):
    # scale pixel values
    face_pixels = face_pixels.astype('float32')
    # standardize pixel values across channels (global)
    mean, std = face_pixels.mean(), face_pixels.std()
    face_pixels = (face_pixels - mean) / std
    # transform face into one sample
    samples = np.expand_dims(face_pixels, axis=0)
    # make prediction to get embedding
    yhat = model.predict(samples)
    return yhat[0]

model = tf.keras.models.load_model('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/weights/facenet_keras.h5')

#Prepare Training Data
x_train=[]
y_train=[]
person_folders=os.listdir('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set/')
#print(person_folders)
# person_rep=dict()
j = 0
for i,person in enumerate(person_folders):
  if person!=".DS_Store":
    print(j)
    # person_rep[j]=person

    image_names=os.listdir('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set/'+person+'/')

    print(image_names)
    for image_name in image_names:
      print(image_name)
      img=load_img('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/data-set/'+person+'/'+image_name,target_size=(160,160))
      img=img_to_array(img)
      img_encode= get_embedding(model, img)
      x_train.append(img_encode.tolist()) # => (128, )
      # print(np.squeeze(img_encode).shape) # => (128, )
      # print(type(np.squeeze(img_encode).tolist())) # => [0, 1, 2, 3, ...., 128]
      y_train.append(person)
    j+=1



#chuyển đổi thành mảng numpy
x_train=np.array(x_train)
y_train=np.array(y_train)

np.save('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/embeddings-folder/img_emb/train_data',x_train)
np.save('/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/embeddings-folder/train_labels',y_train)