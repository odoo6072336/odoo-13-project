from autofaiss import build_index

# build_index(embeddings="addons/hr_attendance_facenet_faiss/static/src/embeddings-folder/a_temp_test_add/img_emb", 
#             index_path="addons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_without_22.index",
#             metric_type="l2",
#             index_infos_path="addons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/index_l2_infos_without_22.json", 
#             max_index_memory_usage="4G",
#             current_memory_available="8G")

# # IP
# build_index(embeddings="addons/hr_attendance_facenet_faiss/static/src/embeddings-folder/img_emb", 
#             index_path="addons/hr_attendance_facenet_faiss/static/src/index-folder/knn.index",
#             metric_type="ip",
#             index_infos_path="addons/hr_attendance_facenet_faiss/static/src/index-folder/index_infos.json", 
#             max_index_memory_usage="4G",
#             current_memory_available="8G")


# # L2
# build_index(embeddings="addons/hr_attendance_facenet_faiss/static/src/embeddings-folder/img_emb", 
#             index_path="addons/hr_attendance_facenet_faiss/static/src/index-folder/knn_l2.index",
#             metric_type="l2",
#             index_infos_path="addons/hr_attendance_facenet_faiss/static/src/index-folder/index_l2_infos.json", 
#             max_index_memory_usage="4G",
#             current_memory_available="8G")

# L2 with embedding_image_no_22.py
build_index(embeddings="/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/embeddings-folder/a_temp_test_add/img_emb",
            index_path="/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/knn_l2_without_22.index",
            metric_type="l2",
            index_infos_path="/Users/haonguyen/Workspace/odoo/v13/odoo-server/myaddons/hr_attendance_facenet_faiss/static/src/index-folder/temp_test_add/index_l2_infos_without_22.json",
            max_index_memory_usage="4G",
            current_memory_available="8G")