from logging import exception
from urllib import request
import cv2
# from facenet_pytorch import MTCNN
from mtcnn import MTCNN
import numpy as np
from face_search import search_face
from PIL import Image
import time



import requests
from datetime import datetime
import datetime



class faceDetect(object):
    def __init__(self, mtcnn):
        self.detector = mtcnn

    def run(self):

        dem = 0

        cap = cv2.VideoCapture(0) #webcam của laptop => 0
        time.sleep(2.0)
        # print(cap.isOpened())

        list_id_e = []

        #vòng lặp đọc các frame image từ camera
        while True:
            ret, frame = cap.read() # returns ret (0/1 if the camera is working) and frames
            try: 
                pil_img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
                pil_img = Image.fromarray(pil_img) # convert to PIL image để crop image

                faces = self.detector.detect_faces(frame)
                print("Results:")
                for face in faces:
                    bounding_box = face['box']
                
                    # draw face frame
                    cv2.rectangle(frame,(bounding_box[0], bounding_box[1]), 
                                (bounding_box[0]+bounding_box[2], bounding_box[1]+bounding_box[3]), 
                                (0,204,0),2)
                    
                    # drop_face
                    crop_img = pil_img.crop((bounding_box[0], bounding_box[1],
                                        bounding_box[0]+bounding_box[2], bounding_box[1]+bounding_box[3]))

                    # đưa crop_img vào search_face
                    id_e, sim = search_face(crop_img)

                    cv2.putText(frame, str(id_e), (bounding_box[0], bounding_box[1]),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv2.LINE_AA)
                    
                    # display similar
                    # cv2.putText(image,str(sim), (bounding_box[2], bounding_box[1]),
                    #         cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

            except Exception as e:
                print(e)
                pass

            # tên window
            # hiển thị khung kết quả.
            cv2.imshow('Face-detector', frame)
            cv2.waitKey(1)

            # nhấn phím q to finish
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break




        cap.release()
        cv2.destroyAllWindows()


    

def main():

    mtcnn = MTCNN()
    rf = faceDetect(mtcnn)
    rf.run()






if __name__ == '__main__':
    main()