from urllib import response
import odoo
import logging
import json
__logger = logging.getLogger(__name__)

import requests
import json
from odoo import http, _, exceptions
from odoo.tests import Form
import werkzeug.wrappers
import xlsxwriter
from odoo.http import content_disposition, request



# get 
class testingRestApi_donthuoc(http.Controller):
    @http.route(['/api/donthuoc_get/'], type = "http", auth="public", methods=['GET'], csrf=False)
    def testing_RestApi_donthuoc_get(self, **params):
        model_name = "don.thuoc"
        # get_id = params.get("id")
        donthuoc = request.env[model_name].sudo().search([])
        dict_donthuoc = {}
        data_donthuoc = []

        for h in donthuoc:
            dict_thuoc = {}
            detail_thuoc = []
            for t in h.thuoc_ids:
                dict_thuoc = {
                    'name': t.name,
                    'nong_do': t.nong_do,
                    'so_luong': t.so_luong,
                    'sang': t.sang,
                    'chieu': t.chieu,
                    'ghi_chu': t.ghi_chu,
                    'don_gia': t.don_gia,
                    'tong_line': t.tong_line
                }
                detail_thuoc.append(dict_thuoc)
            
            dict_donthuoc = {'ma_donthuoc': h.ma_donthuoc, 'ten_benh_nhan': h.name, 'ten_bac_si': h.ten_bacsi.display_name, 
                            'gioi_tinh': h.gioi_tinh, 'ngay_sinh': h.ngay_sinh.strftime('%Y-%m-%dT%H:%M:%S'), 'dia_chi': h.dia_chi,
                            'dien_thoai':h.dien_thoai, 'chan_doan': h.chan_doan, 'ngay': h.ngay.strftime('%Y-%m-%dT%H:%M:%S'),
                            'thuoc': detail_thuoc}
            data_donthuoc.append(dict_donthuoc)

        data ={
            'status': 200,
            'message': "success",
            'response': data_donthuoc
        }

        try:
            return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
                response=json.dumps(data)
            )
        except Exception:
            return werkzeug.wrappers.Response(
                status=400,
                content_type='application/json; charset=utf-8',
                headers=[('Access-Control-Allow-Origin', '*')],
                response=json.dumps({
                    'error': 'Error',
                    'error_descrip': 'Error Description',
                })
            )


    @http.route(['/api/donthuoc_post/'], type = "json", auth="public", methods=['POST'], csrf=False)
    def testing_RestApi_donthuoc_post(self, **params):
        order = params.get("order")
        bn_id = order[0]['bn_id']
        # id_benhnhan = []
        # for b in bn_id:
        #     id_benhnhan.append[b['bn_id']]
        benhnhan_obj = request.env['benh.nhan'].sudo().search([('name', '=', bn_id)])


        chan_doan=order[0]['chan_doan']
        
        ten_bacsi = order[0]['ten_bacsi']
        bacsi_obj = request.env['bac.si'].sudo().search([('name', '=', ten_bacsi)])


        thuoc_ids = order[0]['thuoc_ids']
        vals_line = []
        for t in thuoc_ids:
            thuoc_obj = request.env['nha.thuoc'].sudo().search([('name','=', t['thuoc_id'])])
            # thuoc_obj = request.env['line.thuoc'].sudo().search([('thuoc_id','=', t['thuoc_id'])])
            # (0, 0, {giá trị}) liên kết đến một bản ghi mới cần được tạo bằng từ điển các giá trị đã cho
            vals_line.append((0,0,{
                "thuoc_id": thuoc_obj.id,
                "name": thuoc_obj.name,
                "hoat_chat": thuoc_obj.hoat_chat,
                "nong_do": thuoc_obj.nong_do,
                "sang": t['sang'],
                "chieu": t['chieu'],
                "so_luong": t['so_luong'],
                "don_gia": t['don_gia'],
                "ghi_chu": t['ghi_chu']
            }))
        

        # many2one thì chỉ sử dụng id không sử dụng (6,0,[ids])
        vals_header={
            "chan_doan": chan_doan, "ten_bacsi": bacsi_obj.id, 
            "name": benhnhan_obj.name,"ngay_sinh": benhnhan_obj.ngay_sinh, "bn_id": benhnhan_obj.id , 
            "thuoc_ids": vals_line,
        }

        new_order = request.env['don.thuoc'].sudo().create(vals_header)

        data={
            "status": 200,
            "message": "success",
            "chan_doan": chan_doan,
            "bn_id" : bn_id,
            "ten_bacsi": ten_bacsi,
            "thuoc_ids": thuoc_ids
        }
        return data

